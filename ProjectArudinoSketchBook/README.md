To configure Arduino IDE to compile code within this folder; 
----
 * Set Arduino IDE sketchbook folder to this root directory 
   * i.e. >>FILE>>Preferences, on the Settings tab modify the "Sketchbook location" parameter to this directory.
   * this forces the compiler to first use libraries found in the Libraries folder of this sketchbook folder, if in future you need any more libraries unpack them in the Libraries folder, so they can be versioned with this projects source code. 

Summary of tests found here
-----
gps_device_nano_20Oct21
 * my final build of the software based on my hardware

GPStest 
 * unit test of the GPS 


Unit tests of libraries are found within their subcomponent example folders and may be useful aswell:
for instance, the unit test for FUNCTION1 can be found under "Libraries/FUNCTION1/examples/TST_FUNCTION1.ino"
