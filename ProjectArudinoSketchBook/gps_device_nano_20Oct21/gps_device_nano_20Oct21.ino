#include <TinyGPS.h>
#include <NexStarGPS.h>
#include "soss.h"
#include "ross.h"

#define RX_PIN 3
#define TX_PIN 5

#define SIGNAL_PIN 9
#define LED_PIN 13

ross mountserial(RX_PIN);
soss sendmountserial(TX_PIN);

TinyGPS gps;

NexstarMessageReceiver msg_receiver;
NexstarMessageSender msg_sender(&gps);

int ledState = LOW;             // ledState used to set the LED
long previousMillis = 0;        // will store last time LED was updated

// the follow variables is a long because the time, measured in miliseconds,
// will quickly become a bigger number than can be stored in an int.
long interval_nolock = 150;           // interval at which to blink (milliseconds)
long interval_lock = 5;

boolean haveLock = false;

int haveLock_prev =0; 

void setup()
{
  // GPS module speed
  Serial.begin(9600); // my ublox is at 115200 and spits out nema only on uart1
  pinMode(LED_PIN, OUTPUT);
  digitalWrite(LED_PIN, LOW);
  pinMode(SIGNAL_PIN, OUTPUT);
  digitalWrite(SIGNAL_PIN, LOW);
  pinModeTri(RX_PIN);
  pinModeTri(TX_PIN);
  mountserial.begin(19200);
  msg_receiver.reset();

}

void loop()
{
  unsigned long fix_age;

  float flat, flon;
  unsigned long age, date, time, chars = 0;


  if (mountserial.available())
  {
    int c = mountserial.read();
    //Serial.println(c, HEX);
    if (msg_receiver.process(c))
    {
      if (msg_sender.handleMessage(&msg_receiver))
      {
        digitalWrite(LED_PIN, HIGH);
        mountserial.end();
        delay(100);
        sendmountserial.begin(19200);
        msg_sender.send(&sendmountserial);
        sendmountserial.end();
        pinModeTri(RX_PIN);
        pinModeTri(TX_PIN);
        mountserial.begin(19200);
        digitalWrite(LED_PIN, LOW);
      }
    }
  }

  if (Serial.available())
  {
    char c = Serial.read();
    gps.encode(c);

    gps.get_datetime(NULL, NULL, &fix_age);

    if ((fix_age == gps.GPS_INVALID_AGE) || (fix_age > 5000) || (gps.satellites() == gps.GPS_INVALID_SATELLITES) || (gps.satellites() < 4))
    {
      haveLock = false;
      haveLock_prev =0; // reset the haveloc_prev so we can get final fix printed out 
      
    }
    else
    {
      haveLock = true;
    }

  }

  unsigned long currentMillis = millis();

  if (haveLock)
  {
    if (currentMillis - previousMillis > interval_lock)
    {
      previousMillis = currentMillis;
      static unsigned int val = 21;
      static int dir = 1;
      analogWrite(SIGNAL_PIN, val);
      if ((val == 255) || (val == 20))
      {
        dir = dir * (-1);
      }
      val = val + dir;
      if (haveLock_prev<=5){// report location and time after getitng a fix
        gps.f_get_position(&flat, &flon, &age);
        print_float(flat, TinyGPS::GPS_INVALID_F_ANGLE, 10, 6);
        print_float(flon, TinyGPS::GPS_INVALID_F_ANGLE, 11, 6);
        print_date(gps);
        Serial.println(" ");   
        haveLock_prev++; 
      }
      
    }

  }
  else
  {
    if (currentMillis - previousMillis > interval_nolock)
    {
      // save the last time you blinked the LED
      previousMillis = currentMillis;

      // if the LED is off turn it on and vice-versa:
      if (ledState == LOW)
      {
        ledState = HIGH;
      }
      else
      {
        ledState = LOW;
      }
      gps.f_get_position(&flat, &flon, &age);
      print_float(flat, TinyGPS::GPS_INVALID_F_ANGLE, 10, 6);
      print_float(flon, TinyGPS::GPS_INVALID_F_ANGLE, 11, 6);
      print_date(gps);
      Serial.println(" ");
      //Serial.println(fix_ag);

      digitalWrite(SIGNAL_PIN, ledState);
    }
  }
}

inline void pinModeTri(int pin)
{
  //digitalWrite(pin, LOW);
  //pinMode(pin, OUTPUT);
  pinMode(pin, INPUT);
}


/**********************************************************************
   from tinygps demo
 *********************************************************************/
static void print_int(unsigned long val, unsigned long invalid, int len)
{
  char sz[32];
  if (val == invalid)
    strcpy(sz, "*******");
  else
    sprintf(sz, "%ld", val);
  sz[len] = 0;
  for (int i = strlen(sz); i < len; ++i)
    sz[i] = ' ';
  if (len > 0)
    sz[len - 1] = ' ';
  Serial.print(sz);
  //smartdelay(0);
}


static void print_float(float val, float invalid, int len, int prec)
{
  if (val == invalid)
  {
    while (len-- > 1)
      Serial.print('*');
    Serial.print(' ');
  }
  else
  {
    Serial.print(val, prec);
    int vi = abs((int)val);
    int flen = prec + (val < 0.0 ? 2 : 1); // . and -
    flen += vi >= 1000 ? 4 : vi >= 100 ? 3 : vi >= 10 ? 2 : 1;
    for (int i = flen; i < len; ++i)
      Serial.print(' ');
  }
  //  smartdelay(0);
}


static void print_date(TinyGPS &gps)
{
  int year;
  byte month, day, hour, minute, second, hundredths;
  unsigned long age;
  gps.crack_datetime(&year, &month, &day, &hour, &minute, &second, &hundredths, &age);
  if (age == TinyGPS::GPS_INVALID_AGE)
    Serial.print("********** ******** ");
  else
  {
    char sz[32];
    sprintf(sz, "%02d/%02d/%02d %02d:%02d:%02d ",
            month, day, year, hour, minute, second);
    Serial.print(sz);
  }
  print_int(age, TinyGPS::GPS_INVALID_AGE, 5);
  //  smartdelay(0);
}
