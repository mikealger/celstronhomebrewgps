# Basic idea
 The idea of this project is to document the day or so of effort taken to noodle out a working version of a nextstarGPS based receiver for my Celestron 130SLT computerized scope. My intent was to minimize componets to remove points of failure (i.e. if the nano could power my GPS im going to avoid adding another powersupply). I also wanted to present a slightly cleaner  setup for managing arduino like projects in git
 
# Bill of materials
 
 1. Arduino Nano
 2. gtu7 gps receiver  https://www.newegg.com/p/0N1-018C-00008 or https://www.amazon.ca/Navigation-Satellite-Positioning-Compatible-microcomputer/dp/B07YY82P11/ref=sr_1_8?crid=2429ZA3CO1FW6&dchild=1&keywords=gtu7+gps&qid=1635124179&sprefix=gtu7+gps%2Caps%2C72&sr=8-8
3. 6p4c phone cable, (dollar store should have one just look for a cable with 4 wires)
4. 1n4002 diode (to prevent backflow of power to your scopes power supply)
5. Led and dropper resistor for optional signal on D9 pin
5. General eletronic stuff: breadboard, hookup wire, soldering iron, heat shrink tubing, headerpins things to make your life easier

# Schematic
![Schematic](figures/nanoCelstronGPS_schem.png)
I have also provided this in a frizting file

# Breadboard
![breadboard](figures/nanoCelstronGPS_bb.png)
I have also provided this in a frizting file

## Prototype
The prototype was assembled on a single small breadboard as seen in the following figure. 
![BreadboardAssembled](figures/breadboardassembled.jpg)

### powering the GPS 
I experimented with running the gps off of the 5V regulator of the nano, and the 3.3V regulator, for my final iteration, I left it running on the 5v reg as it seemed to make no difference in the functionality maybe the 5v reg is bigger so I should leave it on that

### Phone cable 
I originally just hacked a cable in half and tinned the ends to jam them into the bread board, I would not recomend this. Insetead fashion a breadboard friendly version by soldering the tinned wires to a right angle header and use heat shrink tubing to prevent shorts. 
![phonecable](figures/phonecable.jpg)


### programing
At first I was confused by the arduino ide having problems programing untill I realized when the gps was powered on it was was constantly sending packets interupting the programing sequence, i included an easily removable wire to facilitate reprograming... again something that could trip you up so i figured id mention it. 

### Software/ Configuraiton of GPS
At first I found that this module worked with ublox ucenter so I promptly changed the baudrate, unfortunatly I didn't realize the baud rate reset on powering off. This is why I left the unit test in my software sketch folder.  maybe I could set it faster and disable usless messages with something like 
https://forum.arduino.cc/t/ubx-protocol-help-configuring-a-neo-6m-arduino-gps-module-fletcher-checksum/226600/6  but 9600 baud should be fast enough for a stationary telescope



