# CelstronHomebrewGPS

A project that contains plans for a home-brew GPS for Celestron scopes based on previous work by Mike Gibbons and Ryoko??
my version pairs down to minimal components and I will hopefully have everything documented for others to get started in building thier own

the project is split into two main folders 
* the first is the source code found in [ProjectArudinoSketchBook](ProjectArudinoSketchBook) where i have copied the needed libraries and have set up a arduino sketchbook so all you need to do is point your arduino sketcbook folder there and it should compile without hunting for libraries on your own. 
* the second is the [documentation folder](ProjectDocuments) where i have the schematics, breadboard layout and my notes along with any hicups i had along the way to help future builders out. 


if you find any of this handy try to replicate the file architecture in your own projects
